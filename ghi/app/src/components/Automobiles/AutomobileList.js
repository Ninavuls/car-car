import { useState, useEffect } from 'react';
import { Link } from "react-router-dom";


const AutomobileList = () => {
  const [autos, setAutos] = useState([]);

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/automobiles/');
    if (response.ok) {
      const data = await response.json();
      setAutos(data.autos);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleDelete = async (e) => {
    const url = `http://localhost:8100/api/automobiles/${e.target.id}`;

    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json"
      }
    };

    const response = await fetch(url, fetchConfig);
    const data = await response.json();

    if (response.ok) {
      getData();
    }
  };

  return (
    <>
      <div className="row">
        <div className="">
          <div className="">
            <h1>Vehicle Models</h1>
            <h2>
              <Link to="new" className="btn btn-success">
                Create an Automobile
              </Link>
            </h2>
            <table className="table table-hover">
              <thead>
                <tr>
                  <th className='text-center'>VIN</th>
                  <th className='text-center'>Color</th>
                  <th className='text-center'>Year</th>
                  <th className='text-center'>Model</th>
                  <th className='text-center'>Manufacturer</th>
                </tr>
              </thead>
              <tbody>
                {
                  autos.map(auto => {
                    return (
                      <tr className='bg-light' key={auto.id}>
                        <td className='text-center'>{auto.vin}</td>
                        <td className='text-center'>{auto.color}</td>
                        <td className='text-center'>{auto.year}</td>
                        <td className='text-center'>{auto.model.name}</td>
                        <td className='text-center'>{auto.model.manufacturer.name}</td>
                      </tr>
                    );
                  })
                }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
};

export default AutomobileList;
