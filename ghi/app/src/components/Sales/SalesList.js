import { useState, useEffect } from 'react';
import { Link } from "react-router-dom";


const SalesList = () => {
  const [salesrecords, setSalesrecords] = useState([]);

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/salesrecords/');
    if (response.ok) {
      const data = await response.json();
      setSalesrecords(data.salesrecords);
    }
  };

  useEffect(() => {
    getData();
  }, []);


  return (
    <>
      <div className="row">
        <div className="">
          <div className="">
            <h1>All Sales</h1>
            <h2>
              <Link to="new" className="btn btn-success">
                Create a sale record
              </Link>
            </h2>
            <table className="table table-hover">
              <thead>
                <tr>
                  <th className='text-center'>Salesperson</th>
                  <th className='text-center'>Employee Number</th>
                  <th className='text-center'>Customer</th>
                  <th className='text-center'>VIN</th>
                  <th className='text-center'>Price</th>
                </tr>
              </thead>
              <tbody>
                {
                  salesrecords.map(salesrecord => {
                    return (
                      <tr className='bg-light' key={salesrecord.id}>
                        <td className='text-center'>{salesrecord.salesperson.name}</td>
                        <td className='text-center'>{salesrecord.salesperson.employee_number}</td>
                        <td className='text-center'>{salesrecord.customer.name}</td>
                        <td className='text-center'>{salesrecord.automobile.vin}</td>
                        <td className='text-center'>{salesrecord.price}</td>
                      </tr>
                    );
                  })
                }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
};

export default SalesList;
