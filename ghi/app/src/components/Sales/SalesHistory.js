import React, { useState, useEffect } from 'react';


function SalesHistory() {
  const [salesrecord, setSalesrecord] = useState([]);
  const [filterTerm, setFilterTerm] = useState([]);

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/salesrecords/');
    if (response.ok) {
      const data = await response.json();
      const salesrecord = data.salesrecords.map((sale) => {
        return {
          salesPerson: sale.salesperson.name,
          customer: sale.customer.name,
          vin: sale.automobile.vin,
          salePrice: sale.price,
        };
      });

      setSalesrecord(salesrecord);

    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleFilterChange = (e) => {
    setFilterTerm(e.target.value);
  };

  return (
    <>
      <div className="my-5 row">
        <div className="offset-1 col-10">
          <div className="shadow p-4 mt-4">
            <h1>Sales History</h1>
            <div className="form-group">
              <label htmlFor="search">Search by Sales Person:</label>
              <input type="text" className="form-control" onChange={handleFilterChange} id="search" />
            </div>
            <table className="table table-hover">
              <thead>
                <tr>
                  <th>Sales Person</th>
                  <th>Customer</th>
                  <th>VIN</th>
                  <th>Sales Price</th>
                </tr>
              </thead>
              <tbody>
                {salesrecord
                  .filter((sale) => sale.salesPerson === filterTerm)
                  .map((sale) => {
                    return (
                      <tr key={sale.salesPerson}>
                        <td>{sale.salesPerson}</td>
                        <td>{sale.customer}</td>
                        <td>{sale.vin}</td>
                        <td>{sale.salePrice}</td>
                      </tr>
                    );
                  })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
};


export default SalesHistory;
