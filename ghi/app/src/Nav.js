import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <NavLink className="navbar-brand" to="/autos">Automobiles</NavLink>
        <NavLink className="navbar-brand" to="/salesrecords">Sales</NavLink>
        <NavLink className="navbar-brand" to="/saleshistory">Sales History</NavLink>
        <NavLink className="navbar-brand" to="/salesperson">Add a Sales Person</NavLink>
        <NavLink className="navbar-brand" to="/customer">Add a Customer</NavLink>
        <NavLink className="navbar-brand" to="/manufactures">Manufacturers</NavLink>
        <NavLink className="navbar-brand" to="/models">Vehicles</NavLink>
        <NavLink className="navbar-brand" to="/technicians">Technician</NavLink>
        <NavLink className="navbar-brand" to="/history">Service History</NavLink>
        <NavLink className="navbar-brand" to="/appointments">Appointments</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
