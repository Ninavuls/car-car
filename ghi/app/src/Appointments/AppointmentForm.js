import React, { useEffect, useState } from 'react';


function AppointmentForm(props) {
  const [technicians, setTechnicians] = useState([]);

  const [vin, setVin] = useState('');
  const [customerName, setCustomerName] = useState('');
  const [reason, setReason] = useState('');
  const [technician, setTechnician] = useState('');
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');


  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.vin = vin;
    data.customer_name = customerName;
    data.date = date;
    data.time = time;
    data.technician = technician;
    data.reason = reason;

    const appointmentUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(appointmentUrl, fetchConfig);

    if (response.ok) {
      const newAppointment = await response.json();

      setCustomerName('');
      setReason('');
      setTechnician('');
      setDate('');
      setTime('');
      setVin('');

    }
  };

  const fetchData = async () => {
    const techUrl = "http://localhost:8080/api/technicians/";
    const response = await fetch(techUrl);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
  };

  const handleCustomerNameChange = (event) => {
    const value = event.target.value;
    setCustomerName(value);
  };

  const handleReasonChange = (event) => {
    const value = event.target.value;
    setReason(value);
  };

  const handleDateChange = (event) => {
    const value = event.target.value;
    setDate(value);
  };

  const handleTimeChange = (event) => {
    const value = event.target.value;
    setTime(value);
  };

  const handleTechnicianChange = (event) => {
    const value = event.target.value;
    setTechnician(value);
  };


  useEffect(() => {
    fetchData();
  }, []);


  return (
    <div className="my-5 container">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add an appointment </h1>
          <form onSubmit={handleSubmit} id="create-technician-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleVinChange}
                value={vin}
                placeholder="vin"
                required
                type="text"
                id="vin"
                className="form-control"
                name="vin"
              />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleCustomerNameChange}
                value={customerName}
                placeholder="customerName"
                required
                type="text"
                id="customerName"
                className="form-control"
                name="customerName"
              />
              <label htmlFor="customerName">Customer Name</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleDateChange}
                value={date}
                placeholder="date"
                required
                type="date"
                id="date"
                className="form-control"
                name="date"
              />
              <label htmlFor="date">Date</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleTimeChange}
                value={time}
                placeholder="time"
                required
                type="text"
                id="time"
                className="form-control"
                name="time"
              />
              <label htmlFor="date">Time</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleReasonChange}
                value={reason}
                placeholder="reason"
                required
                type="text"
                id="reason"
                className="form-control"
                name="reason"
              />
              <label htmlFor="reason">Reason</label>
            </div>

            <div className="mb-3">
              <select
                onChange={handleTechnicianChange}
                value={technician}
                required
                id="technician"
                className="form-select"
                name="technician"
              >
                <option value="">Choose a technician</option>
                {technicians.map((technician) => {
                  return (
                    <option value={technician.employee_number} key={technician.id}>
                      {technician.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-success">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default AppointmentForm;
