import { useState, useEffect } from 'react';
import { Link } from "react-router-dom";


const VehicleModelList = () => {
  const [models, setModels] = useState([]);

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/models/');
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleDelete = async (e) => {
    const url = `http://localhost:8100/api/models/${e.target.id}`;

    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json"
      }
    };

    const response = await fetch(url, fetchConfig);
    const data = await response.json();

    if (response.ok) {
      getData();
    }
  };

  return (
    <>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Vehicles</h1>
            <h2>
              <Link to="new" className="btn btn-success">
                Create a Vehicle
              </Link>
            </h2>
            <table className="table table-hover">
              <thead>
                <tr>
                  <th className='text-center'>Name</th>
                  <th className='text-center'>Manufacturer</th>
                  <th className='text-center'>Picture</th>
                  <th className='text-center'></th>

                </tr>
              </thead>
              <tbody>
                {
                  models.map(model => {
                    return (
                      <tr className='bg-light' key={model.id}>
                        <td className='text-center'>{model.name}</td>
                        <td className='text-center'>{model.manufacturer.name}</td>
                        <td className='text-center'><img src={model.picture_url} width={250} height={100} alt="a pic of a car" /></td>
                        <td className='text-center'><button onClick={handleDelete} id={model.id} className="btn btn-danger">Delete</button></td>
                      </tr>
                    );
                  })
                }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
};

export default VehicleModelList;
