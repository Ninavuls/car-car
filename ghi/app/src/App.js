import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AutomobileList from './components/Automobiles/AutomobileList';
import AutomobileForm from './components/Automobiles/AutomobileForm';
import SalesList from './components/Sales/SalesList';
import SalesForm from './components/Sales/SalesForm';
import SalesHistory from './components/Sales/SalesHistory';
import AddSalesPersonForm from './components/AddSalesPersonForm';
import AddCustomerForm from './components/AddCustomerForm';
import ManufacturerList from './Manufacturers/ManufacturerList';
import ManufacturerForm from './Manufacturers/ManufacturerForm';
import VehicleModelList from './Vehicles/VehicleModelList';
import VehicleModelForm from './Vehicles/VehicleModelForm';
import TechnicianForm from './TechnicianForm';
import AppointmentList from './Appointments/AppointmentList';
import AppointmentForm from './Appointments/AppointmentForm';
import ServiceHistory from './ServiceHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="/autos">
            <Route index element={<AutomobileList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>

          <Route path="/salesrecords">
            <Route index element={<SalesList />} />
            <Route path="new" element={<SalesForm />} />
          </Route>

        <Route path="/saleshistory" element={<SalesHistory />} />

        <Route path="/salesperson" element={<AddSalesPersonForm />} />
        <Route path="/customer" element={<AddCustomerForm />} />

          <Route path="/manufactures">
            <Route index element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>

          <Route path="/models">
            <Route index element={<VehicleModelList />} />
            <Route path="new" element={<VehicleModelForm />} />
          </Route>

          <Route path="/technicians" element={<TechnicianForm />} />
          <Route path="/history" element={<ServiceHistory />} />

          <Route path="/appointments">
            <Route index element={<AppointmentList />} />
            <Route path="new" element={<AppointmentForm />} />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
