from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment
from django.core.serializers.json import DjangoJSONEncoder


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'import_href', 'id', 'vin'
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        'id', 'name', 'employee_number'
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'id', 'customer_name', 'vin', 'technician', 'date', 'time', 'reason', 'is_vip', 'is_finished'
    ]

    encoders = {
        'technician': TechnicianEncoder(),
    }
