from common.json import ModelEncoder
from .models import AutomobileVO, Salesperson, Customer, Salesrecord


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "import_href"]

class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = ["name",]

class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = ["name", "employee_number"]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["name",]

class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "address", "phone_number"]

class SalesrecordListEncoder(ModelEncoder):
    model = Salesrecord
    properties = ["salesperson", "customer", "price", "automobile", "id"]

    encoders = {
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerListEncoder(),
        "salesperson": SalespersonListEncoder(),
    }

class SalesrecordDetailEncoder(ModelEncoder):
    model = Salesrecord
    properties = ["price", "automobile", "customer", "salesperson"]

    encoders = {
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerDetailEncoder(),
        "salesperson": SalespersonDetailEncoder(),
    }
